const express = require('express'),
      app = express(),
      fs = require("fs"),
      path = require("path"),
      multer = require("multer"),
      storage = multer.diskStorage({
        destination: './uploads_tmp/',
        filename: function (req, file, cb) {
            cb(null, file.originalname)
        }
    }),
    upload = multer({
        // Actual upload function using previously defined configuration
        storage: storage
    });

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});

app.post('/', upload.single('file-to-upload'), (req, res) => {
  res.redirect('/');
});

app.listen(3000);